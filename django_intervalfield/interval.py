import operator


class Interval:
    @staticmethod
    def _compare(v1, v2, none):
        if v1 == v2:
            return 0
        if v1 is None:
            return none[0]
        if v2 is None:
            return none[1]
        if v1 < v2:
            return -1
        return 1

    @classmethod
    def _compare_ss(cls, s1, s2):
        return cls._compare(s1, s2, none=(-1, 1))

    @classmethod
    def _compare_se(cls, s, e):
        return cls._compare(s, e, none=(-1, -1))

    @classmethod
    def _compare_es(cls, e, s):
        return cls._compare(e, s, none=(1, 1))

    @classmethod
    def _compare_ee(cls, e1, e2):
        return cls._compare(e1, e2, none=(1, -1))

    @property
    def start_included(self):
        return self._start_included or self.start is None

    @start_included.setter
    def start_included(self, value):
        self._start_included = bool(value)

    @property
    def end_included(self):
        return self._end_included or self.start is None

    @end_included.setter
    def end_included(self, value):
        self._end_included = bool(value)

    def __init__(self, start=None, end=None, start_included=True, end_included=True):
        self.start = start
        self.end = end
        self._start_included = start_included
        self._end_included = end_included

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return False
        return (self.start, self.end, self.start_included, self.end_included) == (
            other.start,
            other.end,
            other.start_included,
            other.end_included,
        )

    def __lt__(self, other):
        if isinstance(other, type(self)):
            if self.end_included and other.start_included:
                op = operator.lt
            else:
                op = operator.le
            return op(self._compare_es(self.end, other.start), 0)
        if self.start is None:
            return False
        op = operator.lt if self.start_included else operator.le
        return op(self.start, other)

    def __le__(self, other):
        if isinstance(other, type(self)):
            return self._compare_es(self.end, other.start) <= 0
        if self.end is None:
            return self.end is None
        return self.end <= other

    def __gt__(self, other):
        if isinstance(other, type(self)):
            if self.start_included and other.end_included:
                op = operator.gt
            else:
                op = operator.ge
            return op(self._compare_se(self.start, other.end), 0)
        if self.end is None:
            return False
        op = operator.lt if self.end_included else operator.le
        return op(self.end, other)

    def __ge__(self, other):
        if isinstance(other, type(self)):
            return self._compare_se(self.start, other.end) >= 0
        if self.start is None:
            return other is None
        return self.start >= other

    def __bool__(self):
        if self.start is None or self.end is None:
            return True
        if self.start != self.end:
            return self.start < self.end
        return self.start_included == self.end_included == True

    def __contains__(self, other):
        if isinstance(other, type(self)):
            if self.start_included or not other.start_included:
                ops = operator.le
            else:
                ops = operator.lt

            if not ops(self._compare_ss(self.start, other.start), 0):
                return False

            if self.end_included or not other.end_included:
                ope = operator.ge
            else:
                ope = operator.gt
            return ope(self._compare_ee(self.end, other.end), 0)
        else:
            op = operator.ge if self.start_included else operator.gt
            if not op(other, self.start):
                return False
            op = operator.le if self.end_included else operator.lt
            if not op(other, self.end):
                return False
            return True

    def __str__(self):
        s = "[" if self.start_included else "]"
        e = "]" if self.end_included else "["
        return f"{s}{self.start}, {self.end}{e}"

    def __repr__(self):
        return str(self)

    def copy(self):
        return type(self)(
            start=self.start,
            end=self.end,
            start_included=self.start_included,
            end_included=self.end_included,
        )

    def merge(self, other):
        if self < other or other > self:
            raise ValueError("Non overlapping intervals")
        if other in self:
            return self
        if self._compare_ss(self.start, other.start) >= 0:
            self.start = other.start
            self.start_included = self.start_included or other.start_included
        if self._compare_ee(self.end, other.end) <= 0:
            self.end = other.end
            self.end_included = self.end_included or other.end_included
        return self

    @classmethod
    def reduce(cls, v1, v2):
        if v1 < v2:
            return [v1, v2]
        if v1 > v2:
            return [v2, v1]
        return [v1.copy().merge(v2)]
