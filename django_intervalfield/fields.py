from django.db import models

from .interval import Interval


__all__ = (
    "BigIntegerIntervalField",
    "DateIntervalField",
    "DateTimeIntervalField",
    "DecimalIntervalField",
    "FloatIntervalField",
    "IntegerIntervalField",
    "IntervalField",
    "IntervalModelAdminMixin",
    "PositiveIntegerIntervalField",
    "PositiveSmallIntegerIntervalField",
    "SmallIntegerIntervalField",
    "TimeIntervalField",
)


class BoundInterval(Interval):
    _NAMES = ("start", "start_included", "end", "end_included")

    def __init__(self, instance, field):
        self._instance = instance
        self._field = field
        super().__init__(
            **{s: getattr(instance, f"{field.name}_{s}") for s in self._NAMES}
        )

    def __setattr__(self, name, value):
        if name in self._NAMES:
            setattr(self._instance, f"{self._field.name}_{name}", value)
        super().__setattr__(name, value)


# We make our field a subclass of property to allow setting its value via the
# model __init__ function.
class IntervalField(models.Field, property):
    data_field_class = None

    def __init__(self, *args, **kwargs):
        self.data_field_class = kwargs.pop("data_field_class", self.data_field_class)
        super().__init__(*args, **kwargs)

    def contribute_to_class(self, cls, name):
        if self.data_field_class is None:
            raise ValueError("data_field_class must be defined")

        self.name = name

        for field_name in ("start", "end"):
            field = self.data_field_class(
                blank=True,
                null=True,
                editable=True,
            )
            including_field = models.BooleanField(
                default=True,
                blank=True,
                editable=True,
                verbose_name=f"{field_name} is included",
            )
            setattr(self, f"{field_name}_field", field)
            cls.add_to_class(f"{self.name}_{field_name}", field)
            setattr(self, f"{field_name}_included_field", including_field)
            cls.add_to_class(f"{self.name}_{field_name}_included", including_field)

        if not hasattr(cls, "_interval_fields"):
            cls._interval_fields = []
        cls._interval_fields.append(self)
        setattr(cls, name, self)

    def get_db_prep_save(self, value):
        pass

    def get_db_prep_lookup(self, lookup_type, value):
        raise NotImplementedError

    def __get__(self, instance, _owner=None):
        if instance is None:
            return self
        return BoundInterval(instance, self)

    def __set__(self, instance, value):
        _NAMES = ("start", "start_included", "end", "end_included")
        if value is None:
            for name in _NAMES:
                setattr(instance, f"{self.name}_{name}", None)
        elif not isinstance(value, Interval):
            raise ValueError
        else:
            for name in _NAMES:
                setattr(instance, f"{self.name}_{name}", getattr(value, name))

    def __str__(self):
        return str(self.interval)


class BigIntegerIntervalField(IntervalField):
    data_field_class = models.BigIntegerField


class DateIntervalField(IntervalField):
    data_field_class = models.DateField


class DateTimeIntervalField(IntervalField):
    data_field_class = models.DateTimeField


class DecimalIntervalField(IntervalField):
    data_field_class = models.DecimalField


class FloatIntervalField(IntervalField):
    data_field_class = models.FloatField


class IntegerIntervalField(IntervalField):
    data_field_class = models.IntegerField


class PositiveIntegerIntervalField(IntervalField):
    data_field_class = models.PositiveIntegerField


class PositiveSmallIntegerIntervalField(IntervalField):
    data_field_class = models.PositiveSmallIntegerField


class SmallIntegerIntervalField(IntervalField):
    data_field_class = models.SmallIntegerField


class TimeIntervalField(IntervalField):
    data_field_class = models.TimeField
