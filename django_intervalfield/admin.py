__all__ = ("IntervalModelAdminMixin",)


class IntervalModelAdminMixin:
    def get_fields(self, *args, **kwargs):
        SUFFIXES = (
            "_start",
            "_start_included",
            "_end",
            "_end_included",
        )

        fields = list(super().get_fields(*args, **kwargs))
        interval_fields = list(getattr(self.model, "_interval_fields", []))

        new_fields = []
        for fieldgroup in fields:
            to_add = True
            if isinstance(fieldgroup, str):
                field = fieldgroup
                for f in interval_fields:
                    for suffix in SUFFIXES:
                        if field == f"{f.name}{suffix}":
                            if suffix == SUFFIXES[0]:
                                new_fields.append(
                                    (
                                        f"{f.name}_start",
                                        f"{f.name}_start_included",
                                    ),
                                )
                                new_fields.append(
                                    (
                                        f"{f.name}_end",
                                        f"{f.name}_end_included",
                                    ),
                                )
                            to_add = False
                            break
                if to_add:
                    new_fields.append(field)
            else:
                # Fields have ben customized by user
                return fields

        return new_fields
