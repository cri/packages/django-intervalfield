import pytest

from ..interval import Interval


class TestInterval:
    def test_equality_empty_1(self):
        assert Interval() == Interval()

    def test_equality_empty_2(self):
        assert Interval() == Interval(None, None)

    def test_equality_empty_3(self):
        assert Interval(None, None) == Interval(None, None)

    def test_equality_1(self):
        assert Interval(1, 10) == Interval(1, 10)

    def test_equality_2(self):
        assert Interval(1, 10) != Interval(1, 10, start_included=False)

    def test_equality_3(self):
        assert Interval(1, 10) != Interval(1, 10, end_included=False)

    def test_contains_scalar_1(self):
        assert 5 in Interval(1, 10)

    def test_contains_scalar_2(self):
        assert 1 in Interval(1, 10)

    def test_contains_scalar_3(self):
        assert 15 not in Interval(1, 10)

    def test_contains_scalar_4(self):
        assert 1 not in Interval(1, 10, start_included=False)

    def test_contains_interval_1(self):
        assert Interval(2, 9) in Interval(1, 10)

    def test_contains_interval_2(self):
        assert Interval(1, 12) not in Interval(1, 10)

    def test_contains_interval_3(self):
        assert Interval(1, 10) in Interval(1, 10)

    def test_contains_interval_4(self):
        assert Interval(2, 10) in Interval(1, 10)

    def test_contains_interval_5(self):
        assert Interval(2, 10) not in Interval(1, 10, end_included=False)

    def test_lt_1(self):
        assert Interval(2, 10) < Interval(11, 13)

    def test_lt_2(self):
        assert not Interval(2, 10) < Interval(10, 13)

    def test_lt_3(self):
        assert Interval(2, 10) < Interval(10, 13, start_included=False)

    def test_le_1(self):
        assert Interval(2, 10) <= Interval(11, 13)

    def test_le_2(self):
        assert Interval(2, 10) <= Interval(10, 13)

    def test_le_3(self):
        assert Interval(2, 10) <= Interval(10, 13, start_included=False)

    def test_gt_1(self):
        assert Interval(11, 13) > Interval(1, 10)

    def test_gt_2(self):
        assert not Interval(10, 13) > Interval(1, 10)

    def test_gt_3(self):
        assert Interval(10, 13, start_included=False) > Interval(1, 10)

    def test_ge_1(self):
        assert Interval(11, 13) >= Interval(1, 10)

    def test_ge_2(self):
        assert Interval(10, 13) >= Interval(1, 10)

    def test_ge_3(self):
        assert Interval(10, 13, start_included=False) >= Interval(1, 10)

    def test_bool_empty(self):
        assert Interval()

    def test_bool_1(self):
        assert Interval(1)

    def test_bool_2(self):
        assert Interval(None, 1)

    def test_bool_3(self):
        assert not Interval(3, 1)

    def test_str_1(self):
        assert str(Interval(1, 10)) == "[1, 10]"

    def test_str_2(self):
        assert str(Interval(1, 10, start_included=False)) == "]1, 10]"

    def test_str_3(self):
        assert str(Interval(1, 10, end_included=False)) == "[1, 10["

    def test_str_4(self):
        assert str(Interval(1, 10, False, False)) == "]1, 10["

    def test_None_must_be_included_1(self):
        assert Interval(start_included=False).start_included == True

    def test_None_must_be_included_2(self):
        assert Interval(end_included=False).end_included == True

    def test_copy(self):
        i1 = Interval(5)
        i2 = i1.copy()
        i1.start = 10
        assert i2.start == 5

    def test_merge_1(self):
        i1 = Interval(1, 5)
        i2 = Interval(4, 10)
        assert i1.merge(i2) == Interval(1, 10)

    def test_merge_2(self):
        i1 = Interval(1, 5)
        i2 = Interval()
        assert i1.merge(i2) == Interval()

    def test_merge_2(self):
        i1 = Interval(1, 5)
        i2 = Interval(5, 10, start_included=False)
        with pytest.raises(ValueError):
            i1.merge(i2)

    def test_merge_3(self):
        i1 = Interval(1, 5)
        i2 = Interval(5, 10)
        assert i1.merge(i2) == Interval(1, 10)

    def test_merge_4(self):
        i1 = Interval(1, 10)
        i2 = Interval(2, 5)
        assert i1.merge(i2) == Interval(1, 10)

    def test_reduce_1(self):
        i1 = Interval(1, 10)
        i2 = Interval(2, 5)
        assert Interval.reduce(i1, i2) == [Interval(1, 10)]

    def test_reduce_2(self):
        i1 = Interval(1, 8, start_included=False)
        i2 = Interval(2, 10)
        assert Interval.reduce(i1, i2) == [Interval(1, 10, start_included=False)]

    def test_reduce_3(self):
        i1 = Interval(1, 8)
        i2 = Interval(9, 10)
        assert Interval.reduce(i1, i2) == [Interval(1, 8), Interval(9, 10)]
